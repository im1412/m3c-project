M3C 2016 Project by Irina Moraru 00757578

part1 contains -network.f90 (modified ,added subroutine adjacency_list,commented subroutine connectivity subroutine vary_connectivity)
               -p11.png image to ilustrate evolution of nodes visited 
               -p12.png image to ilustrate dependency of Fn(Ntime,X0) on Nm , because i used adj matrix instead of the lists couldnt run it with Nm very big 
               -part1.f90 fortran code contains module rwmodule to generate random walks on a network,had to write dimension for Xm which is output
               -part1.py python code to produce different statistics for the random walks Nm everywhere instead of m generates p11,p12
part2 contains -network.f90 (same as in part1)
               -part2.f90 fortran code contains flunet to compute rhs for epidemics spreading on complex network model which will be solved in python .computes rhs and rhs omp which is parallelised with openmp 
               -part2.py python code solves the system of odes with IC using own rhs computed then rhs and rhs omp from part2.f90 then computes different statistics such as max fraction of contagious population and also analyses performance of code with different computations of rhs  
               -p21.png plot of maximum contagious population(fraction) considering different w
               -p22.png plot of time at which it reaches cmax vs w
               -p23.png plots nmax versus w
               -p261.png plots time of python approach for rhs with size of network(Nt) increasing 
               -p262.png plot of speedup of fortran rhs ,Nt increasing 
               -p263.png plot of speedup of fortran +openmp 

part3 contains -part31.f90 fortran code using rk4 time marching method to compute solution and order at each step(rk4 routine) and  rhs in rhs subroutine
               -part32.py contains  function oscillator that generates some variables needed by the fortran module and also after getting the solution and order from fortran plots dependency of order on c
