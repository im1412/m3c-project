!
!Project part 1 by Irina Moraru 00757578
!f2py --f90flags='-fopenmp' -c network.f90 part1.f90 -m rw
module rwmodule
    use network
    use omp_lib

contains

subroutine rwnet(Ntime,Nm,X0,N0,L,Nt,isample,X,XM)
    !random walks on a recursive network
    !Input variables:
    !Ntime: number of time steps
    !Nm: number of walks
    !X0: initial node, (node with maximum degree if X0=0)
    !N0,L,Nt: recursive network parameters
    !Output: X, m random walks on network each with Ntime steps from initial node, X0
    !XM: fraction of walks at initial node, computed every isample timesteps
    implicit none
    integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample
    integer, dimension(Ntime+1,Nm), intent(out) :: X
    real(kind=8), dimension(Ntime/isample+1), intent(out)::Xm
    
    real(kind=8), allocatable,  dimension(:) :: P
    real(kind=8), allocatable,  dimension(:,:) :: R,CP
    real(kind=8), dimension(Ntime) :: Xm_alltime
     
    integer, dimension(N0+Nt):: qnet
    integer, dimension(N0+L*Nt,2) :: enet
    integer, dimension(N0+Nt,N0+Nt) :: anet
    
    integer:: node,j1,j,i,m,i_add,N,qmax,a,x1,d,b
    
    call generate(N0,L,Nt,qmax,qnet,enet)
    call adjacency_matrix(N0+Nt,enet,anet)
    allocate(P(N0+Nt))
    allocate(CP(N0+Nt,2))
    allocate(R(Ntime+1,Nm))
    call random_number(R)
    
    N=N0+Nt
   
    if(X0==0) then
        X(1,:)=maxloc(qnet,1)
    else
        X(1,:)=X0
    end if  
    !I chose to use adj matrix instead of adj lists.
    !did the assigning of next node to be walked on by regarding cumulative probability following example from homework 4 solutions 
    do j=1,Nm
        node = X(1,1)
        do i=2,Ntime+1
            b=1
            do m=1,N
                if (anet(node,m)/=0) then
                    P(b)=dble(anet(node,m))/qnet(node)
                    CP(b,1)=m
                    b=b+1  
                end if 
            end do
            b=b-1
            CP(1,2)=P(1)
            CP(b,2)=1.0
            do j1=2,b-1
                CP(j1,2) = CP(j1-1,2) + P(j1)
            end do	
            i_add = minloc(abs(CP(1:b,2)-R(i,j)),1)
            if (R(i,j)>CP(i_add,2))then
                i_add = i_add + 1
            end if
            X(i,j)=CP(i_add,1)
            node=CP(i_add,1)    
        end do
    end do

    Xm_alltime(i)=0.d0
    
    do i=2,Ntime+1,isample
        a=0 
        do j=1,Nm
            if (X(i,j)==X(1,1))then
                a=a+1
            end if 
            Xm_alltime(i-1)=dble(a)/Nm
        end do
    end do
    
    
    Xm=Xm_alltime(1:Ntime+1:isample)
    

    deallocate(R,CP,P)
end subroutine rwnet


subroutine rwnet_omp(Ntime,Nm,X0,N0,L,Nt,isample,numthreads,X,XM)
    !parallelized version of rwnet, parallel regions should
    !use numthreads threads
    !use omp_lib	
    implicit none
    integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample,numthreads
    integer, dimension(Ntime+1,Nm), intent(out) :: X
    real(kind=8), dimension(Ntime/isample+1), intent(out)::Xm
    
    real(kind=8), allocatable,  dimension(:) :: P
    real(kind=8), allocatable,  dimension(:,:) :: R,CP
    real(kind=8), dimension(Ntime) :: Xm_alltime
     
    integer, dimension(N0+Nt):: qnet
    integer, dimension(N0+L*Nt,2) :: enet
    integer, dimension(N0+Nt,N0+Nt) :: anet
    
    integer:: node,j1,j,i,m,i_add,N,qmax,a,x1,d,b
    
    call generate(N0,L,Nt,qmax,qnet,enet)
    call adjacency_matrix(N0+Nt,enet,anet)
    allocate(P(N0+Nt))
    allocate(CP(N0+Nt,2))
    allocate(R(Ntime+1,Nm))
    call random_number(R)
    
    N=N0+Nt
   
    if(X0==0) then
        X(1,:)=maxloc(qnet,1)
    else
        X(1,:)=X0
    end if  
    
    !$OMP parallel do private(node,i,b,m,P,CP,j1,i_add)
    do j=1,Nm
        node = X(1,1)
        a=0
        do i=2,Ntime+1
            b=1
            do m=1,N
                if (anet(node,m)/=0) then
                    P(b)=dble(anet(node,m))/qnet(node)
                    CP(b,1)=m
                    b=b+1  
                end if 
            end do
            b=b-1
            CP(1,2)=P(1)
            CP(b,2)=1.0
            do j1=2,b-1
                CP(j1,2) = CP(j1-1,2) + P(j1)
            end do	
            i_add = minloc(abs(CP(1:b,2)-R(i,j)),1)
            if (R(i,j)>CP(i_add,2))then
                i_add = i_add + 1
            end if
            X(i,j)=CP(i_add,1)
            node=CP(i_add,1)    
        end do
    end do
    !$OMP end parallel do

    Xm_alltime(i)=0.d0
    !$OMP parallel do private(j,a)
    do i=2,Ntime+1,isample
        a=0 
        do j=1,Nm
            if (X(i,j)==X(1,1))then
                a=a+1
            end if 
            Xm_alltime(i-1)=dble(a)/Nm
        end do
    end do
    !$OMP end parallel do
    
   
    Xm=Xm_alltime(1:Ntime+1:isample)
   
    deallocate(R,CP,P)

end subroutine rwnet_omp


end module rwmodule
