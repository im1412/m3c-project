"""Project, part 1 by Irina Moraru 00757578"""
import numpy as np
import matplotlib.pyplot as plt
from rw import rwmodule as rw
#Assumes rwmodule has been compiled with f2py to produce rw.so


def analyze_rnet(Ntime,Nm,X0,N0,L,Nt,display):
    """Input variables:
    Ntime: number of time steps
    m: number of walks
    X0: initial node, (node with maximum degree if X0=0)
    N0,L,Nt: recursive network parameters
    """
        
    X,Xm=rw.rwnet_omp(Ntime,Nm,X0,N0,L,Nt,isample,2)
    #already takes the node with max degree in rwnet so need to write another if statement here
    P=np.zeros((Ntime+1,N0+Nt+1))
    
   
    
    for i in range(0,Ntime+1):
        y=np.bincount(X[i,:])
        z=len(y)
        for j in range(z):
    
            P[i][j]=P[i][j]+y[j]
           
   
    P=P/float(Nm)
  
    
    
    t=np.arange(0,Ntime+1)
    C=np.zeros(Ntime+1)
    for i in range(0,Ntime+1):
        C[i]=np.argmax(P[i,:])+1     
    #print C
    if display:
        plt.figure()
        plt.plot(t,C)
        plt.xlabel('t')
        plt.ylabel('node most visited')
        plt.title('Evolution of popular visited nodes(Irina Moraru, analyze_rnet)')
        plt.show()

    return P



def convergence_rnet(Ntime,Nm,X0,N0,L,Nt,display):
    """Input variables:
    Ntime: number of time steps
    m: number of walks
    X0: initial node, (node with maximum degree if X0=0)
    N0,L,Nt: recursive network parameters
    """
    #this function tells us in how many walks we are converging back to the original node 
    t=np.arange(1,Nm+1)
    F=np.zeros(Nm+1)
    for i in range(1,Nm+1):
       
        X,XM = rw.rwnet_omp(Ntime,i,X0,N0,L,Nt,1,2)
        y = 0
        x0=X[0,0]
        for j in range(i):
            if X[Ntime,j]==x0:
                y = y+1
        F[i]=y/float(i)
    
    if display:
        plt.figure()
        plt.plot(t,F[1:])   
        plt.xlabel('Nm')
        plt.ylabel('F')
        plt.title('dependency of F(Ntime,X0)on Nm (Irina Moraru, analyze_rnet)')
        plt.show()
 

#tried smaller numbers for plotting since I wasn't sure if it was supposed to take that long to plot


    	
if __name__== '__main__':
#add code here to call functions and generate figures
    N0,L,Nt,Ntime,Nm,X0,isample=5,2,200,1000,100,0,2
    #k=analyze_rnet(Ntime,Nm,X0,N0,L,Nt,True)
    l=convergence_rnet(Ntime,Nm,X0,N0,L,Nt,True)
        
