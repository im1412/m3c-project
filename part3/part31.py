#part3.1 by Irina Moraru 00757578
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from p3 import sync #assumes that fortran module sync has been compiled with f2py as p3.so



def oscillator(Nt,T,N,c,s,mu=1):
    """Simulate fully-coupled network of oscillators
    Compute simulation for Nt time steps between 0 and T (inclusive)
    input: N: number of oscillators
           c: coupling coefficient
           mu,sigma: distribution parameters for omega_i
    """
   #sharing the variables with fortran,sampling w from normal distribution
    sync.ntotal=N
    sync.c=c
    sync.w=np.random.normal(mu,s,N)
    #creating timespace and initial condition from uniform distribution 
    t0=0.0
    t=np.linspace(0,T,Nt+1)
    y0=np.random.uniform(0,2*np.pi,N)
    dt=float(T)/Nt
    #getting the results calculated from fortran
    theta,order=sync.rk4(t0,y0,dt,Nt)
    mod = np.mod(theta[:,Nt], 2*np.pi)
    
    plt.figure()
    plt.plot(np.arange(1,N+1),mod)
    plt.title("dependency of order on c \n oscillator, Irina Moraru")
    plt.xlabel('c')
    plt.ylabel('order')
    plt.show()
    
    return t,sync.w,y0,theta,order
    
if __name__ == '__main__':
    n,c,m,s = 101,10.0,1.0,0.1
    Nt,T = 500,100
    t,omega,theta0,theta,order = oscillator(Nt,T,n,c,s,m)
