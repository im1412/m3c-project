"""Project, part 2 by Irina Moraru 00757578"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from n1 import network as net
from p1 import flunet as fn
from time import clock,time


def initialize(N0,L,Nt,pflag):
    """Generate network, and initial conditions
    If pflag is true, construct and return transport matrix
    """
    qmax,qnet,enet=net.generate(N0,L,Nt)
    ii=np.argmax(qnet)
    #ii=initial infected index
    N=N0+Nt
    I = np.zeros(3*N)
    for i in range(N):
        I[i]=1
    I[ii] = 0.1
    I[ii+N] = 0.05
    I[ii+2*N] = 0.05    

    if pflag:
        A=net.adjacency_matrix(N,enet)
        P=np.zeros((N,N))
        for i in range(N):
            for j in range(N):
                P[i][j]=qnet[i]*A[i][j]
                sum=np.dot(A[:,j],qnet)
                if (sum==0):
                   P[i][j]=0
                else:
                   P[i][j]=(P[i][j])/float(sum)
        return I,ii,P
    else:
        return I,ii


def solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Nt,call):
    """Simulate network flu model at Ntime equispaced times
    between 0 and T (inclusive). Add additional input variables
    as needed clearly commenting what they are and how they are  
    used
    """
   #added n0,nt necessary to know the total of nodes
   #added call variable with possible values 0,1,2 so we can compute the odeint with different rhs useful for performance  
    
    
    #add input variables to RHS functions if needed
    def RHSnet(y,t,a,b0,b1,g,k,w,N0,Nt,P):
        """RHS used by odeint to solve Flu model"""
        b = b0 + b1*(1.0+np.cos(2.0*np.pi*t))
        N=N0+Nt
        
        S=y[0:N]
        E=y[N:2*N]
        C=y[2*N:3*N] 
    
        #had to vectorize my code since it was taking a long time for the code to run through all the loops.
        #made a function to return an array full with sum_i so i wouldnt have to write it everytime for every ode called it later with S,E,C instead of X
        def sum(X,N):
            sum=np.zeros(N)
            for i in range(N):
                sum[i]=0
                for j in range(N):
                    sum[i]=sum[i]+(P[i][j]*X[j]-P[j][i]*X[i])
            return sum

        sumS=sum(S,N)
        sumE=sum(E,N)
        sumC=sum(C,N)
       
        dy = np.zeros(3*N)
        for i in range(N):
            dy[i]=k*(1-S[i])-b*C[i]*S[i]+w*sumS[i]
            dy[i+N]=b*C[i]*S[i]-(k+a)*E[i]+w*sumE[i]
            dy[i+2*N]=a*E[i]-(g+k)*C[i]+w*sumC[i]
        
        return dy
    
    def RHSnetF(y,t,a,b0,b1,g,k,w,N0,Nt,P):
        """RHS used by odeint to solve Flu model"
        Calculations carried out by fn.rhs
        """
        dy = fn.rhs(N0+Nt,y,t,a,b0,b1,g,k,w,P)
        return dy
        
    def RHSnetFomp(y,t,a,b0,b1,g,k,w,N0,Nt,P):
        """RHS used by odeint to solve Flu model
        Calculations carried out by fn.rhs_omp
        """
        #numthreads=2 since thats what i remember we were advised to use for last project 
        dy = fn.rhs_omp(N0+Nt,y,t,a,b0,b1,g,k,w,2,P)
        return dy

    #Add code here and to RHS functions above to simulate network flu model
    t=np.linspace(0,T,Ntime+1)
    N=N0+Nt
    y0,ii,P=initialize(N0,L,Nt,True)
    #now we use call variable to use different rhs 
    if call==0:
        Y = odeint(RHSnet,y0,t,args=(a,b0,b1,g,k,w,N0,Nt,P))
    elif call==1:
        Y = odeint(RHSnetF,y0,t,args=(a,b0,b1,g,k,w,N0,Nt,P))
    else:
        Y = odeint(RHSnetFomp,y0,t,args=(a,b0,b1,g,k,w,N0,Nt,P))
    #separating S,E,C from original matrix solution y
    S,E,C= Y[:,0:N],Y[:,N:2*N],Y[:,2*N:3*N]

    return t,S,E,C
 

def analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,threshold,warray,display=False):
    """analyze influence of omega on: 
    1. maximum contagious fraction across all nodes, Cmax
    2. time to maximum, Tmax
    3. maximum fraction of nodes with C > threshold, Nmax    
    Input: N0,L,Nt: recursive network parameters 
           T,Ntime,a,b0,b1,g,k: input for solveFluNet
           threshold: use to compute  Nmax
           warray: contains values of omega
    """
    N=N0+Nt
    l=len(warray)
    Cmax=np.zeros(l)
    Tmax=np.zeros(l)
    Nmax=np.zeros(l)
    i=0
    y0,ii,P=initialize(N0,L,Nt,True)
    for w in warray:
        t,S,E,C=solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Nt,2) 
        Cmax[i]=np.max(C)
        Tmax[i]=(np.argmax(C))/float(N)
        #i dont think my nmax is working 
        for j in range(Ntime+1):
            count=0
            for k in range(N):
                if C[j,k]>threshold:
                    count=count+1
            count=float(count)/N
            if (Nmax[i]<count):
                Nmax[i]=count
 
        i=i+1
     
    if display:
        plt.figure()
        plt.plot(warray,Cmax)
        plt.title(" maximum contagious population  \n analyse, Irina Moraru")
        plt.xlabel('w')
        plt.ylabel('Cmax')
        plt.show()
    
        plt.figure()
        plt.plot(warray,Tmax)
        plt.title("Fraction of maximum time  \n analyse, Irina Moraru")
        plt.xlabel('w')
        plt.ylabel('Tmax')
        plt.show()

        plt.figure()
        plt.plot(warray,Nmax)
        plt.title("Nmax  \n analyse, Irina Moraru")
        plt.xlabel('w')
        plt.ylabel('Nmax')
        plt.show()
    
    return Cmax,Tmax,Nmax
    #for large Nt cmax just stays at initial value

def visualize(enet,C,threshold):
    """Optional, not for assessment: Create figure showing nodes with C > threshold.
    Use crude network visualization algorithm from homework 3
    to display nodes. Contagious nodes should be red circles, all
    others should be black"""
    return None


#def performance(Nt,m):
def performance(N0,L,Ntarray,T,Ntime,a,b0,b1,g,k,w,y0,P,display=False):
    """function to analyze performance of python, fortran, and fortran+omp approaches
        Add input variables as needed, add comment clearly describing the functionality
        of this function including figures that are generated and trends shown in figures
        that you have submitted
    """
   #first plot is an increasing function so as the network becomes larger so does increase the time to compile. it was expected 
   #second plot shows we obtain a speedup using fortran compared to python  and then its again decreasing 
   #third one shows the same an increasing speedup and then when we increase nt >50 it starts decreasing 
   #tried to choose a reasonable number for nt since i had so many problems waiting for it to plot because of my for loops so as soon as i saw a decrease in speedup i think i am showing the trend  
    s = np.zeros(np.size(Ntarray))
    l = np.zeros(np.size(Ntarray))
    t = np.zeros(np.size(Ntarray))
    x = np.zeros(np.size(Ntarray))
    y = np.zeros(np.size(Ntarray))
    
    for i in range(np.size(Ntarray)):
      
        t0=time()
        solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Ntarray[i],0)
        t1=time()
        s[i]=t1-t0  
    for i in range(np.size(Ntarray)):
        
        t0=time()
        solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Ntarray[i],1)
        t1=time()
        l[i]=t1-t0
    for i in range(np.size(Ntarray)):
       
        t0=time()
        solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Ntarray[i],2)
        t1=time() 
        t[i]=t1-t0

        
    for i in range(np.size(Ntarray)):
        x[i]=s[i]/l[i]
        y[i]=s[i]/t[i]
        
    if display:
        plt.figure()
        plt.plot(Ntarray,s)
        plt.title("speed of Python approach vs Nt, \n performance, Irina Moraru")
        plt.xlabel('Nt')
        plt.ylabel('time of Python approach')
        plt.show()
        
        plt.figure()
        plt.plot(Ntarray,x)
        plt.title("speedup of Fortran approach vs Nt,\n performance, Irina Moraru")
        plt.xlabel('Nt')
        plt.ylabel('speedup of Fortran approach')
        plt.show()
        
        plt.figure()
        plt.plot(Ntarray,y)
        plt.title("speedup of Fortran+OpenMP approach vs Nt,\n performance, Irina Moraru")
        plt.xlabel('Nt')
        plt.ylabel('speedup of Fortran+OpenMP approach')
        plt.show()

       
    return None 

if __name__ == '__main__':            
    a,b0,b1,g,k,w = 45.6,750.0,0.5,73.0,1.0,0.1
    N0,L,Nt,Ntime=5,2,10,10
    T=2
    threshold=0.1
    y0,ii,P=initialize(N0,L,Nt,True)

  
    #y=solveFluNet(T,Ntime,a,b0,b1,g,k,w,y0,N0,Nt,2)
   

    warray=np.array([0.0,0.02,0.1,0.2,0.5,1.0])
    #c,t,n=analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,threshold,warray,True)
    
    Ntarray = np.array([10,20,30,40,50,60])
    performance(N0,L,Ntarray,T,Ntime,a,b0,b1,g,k,w,y0,P,True)
