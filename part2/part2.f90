!part 2 by Irina Moraru 00757578
module flunet
    use omp_lib
    implicit none
    !add variables as needed
    save
    contains


!subroutine rhs(n,y,t,a,b0,b1,g,k,w,dy,P)
subroutine rhs(n,y,t,a,b0,b1,g,k,w,P,dy)
    implicit none
    !Return RHS of network flu model
    !input: 
    !n: total number of nodes
    !y: S,E,C
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !output: dy, RHS
    !needed P
    integer, intent(in) :: n
    real(kind=8), dimension(n*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(n,n),intent(in)::P
    real(kind=8), dimension(n*3), intent(out) :: dy
    
    !f2py depend(n) y, P, dy
    !got errors f2pying it read on stackoverflow sometimes i need to add this line to indicate dependence 
    
    real(kind=8), dimension(n):: S,E,C,ds,de,dc
    integer::i,j
    real(kind=8)::sum1,sum2,sum3,b,pi
    
    pi = acos(-1.d0)
    b = b0 + b1*(1.0+cos(2.d0*pi*t))
    S=y(1:n)
    E=y(n+1:2*n)
    C=y(2*n+1:3*n)
   
    do i=1,n
        sum1=0 
        sum2=0
        sum3=0
        do j=1,n
            sum1=sum1+P(i,j)*S(j)-P(j,i)*S(i)
            sum2=sum2+P(i,j)*E(j)-P(j,i)*E(i)
            sum3=sum3+P(i,j)*C(j)-P(j,i)*C(i)
        end do
        ds(i)=k*(1-S(i))-b*C(i)*S(i)+w*sum1
        de(i)=b*C(i)*S(i)-(k+a)*E(i)+w*sum2
        dc(i)=a*E(i)-(g+k)*C(i)+w*sum3    
    end do
    
    dy(1:n)=ds
    dy(n+1:2*n)=de
    dy(2*n+1:3*n)=dc
     
end subroutine rhs



subroutine rhs_omp(n,y,t,a,b0,b1,g,k,w,numthreads,P,dy)
    implicit none 
     !Return RHS of network flu model, parallelized with OpenMP
    !input: 
    !n: total number of nodes
    !y: S,E,C
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !numthreads: the parallel regions should use numthreads threads
    !output: dy, RHS
    integer, intent(in) :: n,numthreads
    real(kind=8), dimension(n*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(n,n),intent(in):: P
    real(kind=8), dimension(n*3), intent(out) :: dy
    
    !f2py depend(n) y, P, dy
    
    real(kind=8), dimension(n):: S,E,C,ds,de,dc
    integer::i,j
    real(kind=8)::sum1,sum2,sum3,b,pi
    
    pi = acos(-1.d0)
    b = b0 + b1*(1.0+cos(2.d0*pi*t))
    S=y(1:n)
    E=y(n+1:2*n)
    C=y(2*n+1:3*n)
    
    !$OMP parallel do private(j,sum1,sum2,sum3)
    do i=1,n
        sum1=0 
        sum2=0
        sum3=0
        do j=1,n
            sum1=sum1+P(i,j)*S(j)-P(j,i)*S(i)
            sum2=sum2+P(i,j)*E(j)-P(j,i)*E(i)
            sum3=sum3+P(i,j)*C(j)-P(j,i)*C(i)
        end do
        ds(i)=k*(1-S(i))-b*C(i)*S(i)+w*sum1
        de(i)=b*C(i)*S(i)-(k+a)*E(i)+w*sum2
        dc(i)=a*E(i)-(g+k)*C(i)+w*sum3    
    end do
    !$OMP end parallel do
    
    dy(1:n)=ds
    dy(n+1:2*n)=de
    dy(2*n+1:3*n)=dc


end subroutine rhs_omp





end module flunet
